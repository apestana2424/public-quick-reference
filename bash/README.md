Here are some useful bash shortcuts. Many of these shortcuts also work on other popular Linux shells.

If you don't know which shell you're using, use the following command to find out: `echo "$SHELL"`

|Action|Shortcut|Notes|
|-|-|-|
|Move to the previous command in history.|CTRL + p||
|Move to the next command in history.|CTRL + n||
|Search command history. (Perform the shortcut and start typing.)|CTRL + r|Perform the shortcut again to iterate through the results.|
|Exit search in command history.|CTRL + g||
|Reset changes (to as it was in history).|ALT + r||
|Move cursor to the beginning.|CTRL + a||
|Move cursor to the end.|CTRL + e||
|Move cursor back one word.|ALT + b||
|Move cursor forward one word.|ALT + f||
|Cancel the current command (if you haven't pressed ENTER yet, you can also use it to "clear the command line").|CTRL + c||
|Clear the screen.|CTRL + l||