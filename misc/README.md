# Access an AWS VM (EC2 instance) Through SSH Authenticating With a Private Key (.pem file)

Assuming you are using a Linux machine, on the terminal do:

`ssh -i <path to .pem file> <user>@<host>`

For example:

`ssh -i ./misc/xyz.pem centos@xyz.atale.io`

If you get an error such as the following:

```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Permissions 0644 for './misc/xyz.pem' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
Load key "./misc/xyz.pem": bad permissions
centos@xyz.atale.io: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
```

It means you need to change permissions on the .pem file like this:

`chmod 400 misc/xyz.pem`

# Expose an AWS VM (EC2 instance) to the Internet Through a Fixed IP Address

In the AWS Management Console, create (allocate) an Elastic IP address (this is an AWS feature), then associate it to the desired VM.

# Allow/Deny Traffic to Ports of an AWS VM (EC2 instance)

In the AWS Management Console, go to the desired VM configuration page and in the Security tab click the Security Group link. Then, create inbound (this is the most usual) or outbound rules as required.

# Allow/Deny SSH Access Just With Password to a Linux Machine

Do:

`sudo vi /etc/ssh/sshd_config`

Change the lines below accordingly:

```
PasswordAuthentication yes
#PasswordAuthentication no
```

Then do (for changes to the configuration to take effect):

`sudo systemctl restart sshd`

# Create a Microsoft SQL Server Instance on AWS’s RDS (Managed Relational Database Service)

Check our article on Medium: [Create a Microsoft SQL Server Instance on AWS’s RDS (Managed Relational Database Service)](https://medium.com/@ataledotio/create-a-microsoft-sql-server-instance-on-awss-rds-managed-relational-database-service-d5466a9c9cfb)

# Google Play Console - App in the Internal Tests lane doesn't get pushed automatically to testers.

If you have an app in the Internal Tests lane and you are wondering why app updates are not installed automatically in the tester's devices, and why they don't see the button to update the app when they access the app screen in the Play Store, it's probably because the app hasn't been reviewed and approved by Google yet.
