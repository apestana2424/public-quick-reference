Here are some useful Linux commands.

|Command|Description|Notes|
|-|-|-|
|`man <command>`|Show the manual pages for a command.|Press `q` to exit.|
|`pwd`|Show current directory (full path).||
|`ls -l`|List the contents of a directory.||
|`cd <directory>`|Change directory.|Use `cd ..` to go up (or down, depending on how you look at it) one directory.|
|`cat <filename>`|Show the contents of a file.||
|`tail <filename>`|Show the contents at the end of a file.|This is useful, for example, to quickly check the last lines of a log file.|
|`less <filename>`|Show the contents of a file, being able to navigate up and down, among other operations.|Press `q` to exit.|
|`df -H`|Show disk space usage.||
|`top`|Shows processor usage and other info (this is oversimplifying what this command does).|Press `q` to exit.|
