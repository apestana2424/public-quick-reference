# Service Control
Reload configuration for the nginx service (for example, after changes to configuration files such as /etc/nginx/nginx.conf):

`systemctl reload nginx`

# Configure nginx to redirect www to non-www

Edit redirect.conf (it may not exist, just create it) in /etc/nginx/conf.d:

`vi /etc/nginx/conf.d/redirect.conf`

In the file, place:

```
server {
    server_name www.<domain name>;
    return 301 $scheme://<domain name>.com$request_uri;
}
```

If you want to redirect to https straight away, insted of using the variable $scheme, place https:

```
server {
    server_name www.<domain name>;
    return 301 https://<domain name>$request_uri;
}
```

Then reload the nginx configuration:

`systemctl reload nginx`

# Configure nginx to use custom error pages

Edit /etc/nginx/nginx.conf:

`vi /etc/nginx/nginx.conf`

In the appropriate server section place, for example, for the 404 error (adjust path and filename):

```
error_page 404 /404.html;
location = /404.html {
}
```

Then reload the nginx configuration:

`systemctl reload nginx`

# Install Let's Encrypt SSL certificate

Install utilities:

`dnf install epel-release`

`dnf install certbot python3-certbot-nginx`

Generate and install the certificate:

`certbot --nginx -d <domain name>`

Then reload the nginx configuration:

`systemctl reload nginx`

# Misc

## Suggested permissions for website directories and files

Directories (assuming you are in the website root):

`find ./ -type d -exec chmod 755 {} \;`

Files (assuming you are in the website root):

`find ./ -type f -exec chmod 644 {} \;`
