After you create a project (Scrum or Kanban) the Deployments section does not show - it will start showing after the project is connected with the repository (doing a commit/push to the repository referencing one of the issues of the project, assuming the accounts [Jira and Bitbucket] are already connected).

After creating a Kanban project, the Backlog section does not show by default (you needed it, for example, to associate issues to releases) - go to the board, access the "Board settings" and then drag one of the status (probably BACKLOG) to the "Kanban backlog" area. Also, note the "Epics panel" above, which determines how epics show in the Backlog section.

A project role (the role you specify when you add a team member to a project) is different from a user group. You create project roles by accessing "Gear (on top right)" -> "System (under Jira Settings)" -> "Project Roles (under Security)". Then, you need to assign permissions to the project role by accessing "Gear (on top right)" -> "Issues (under Jira Settings)" -> "Permission schemes (under Issue Attributes)".

By default, Tasks and Bugs do not use the "Story Points" field. To use that field in those type of issues, access "Gear (on top right)" -> "Issues (under Jira Settings)" -> "Custom Fields (under Fields)", then search for "Story Points", then edit the context (a few more steps required).

It's not a good ideia to delete issues, it's best to changed them to a final state (for example, we had problems with an app from the Jira marketplace because we deleted an issue in a project and it kept showing in the app and we were unable to do anything with those issues in the app).

https://community.atlassian.com/t5/Jira-Software-questions/Story-point-and-story-point-estimate-duplicate-fields/qaq-p/904742: Jira has one specific field to track each project type and a different field will not work:

- On Classic projects, the estimation is measured on the Story Points field.

- On Next-gen projects, the estimation is measured on the Story Point Estimate field.

At least in Classic projects, the estimation can be changed from Story Points to Original Time Estimate (this is done in the Estimation tab of the Board Settings).

Field Configuration vs Field Configuration Scheme: A Field Configuration Scheme associates a Field Configuration to certain issue types of a project.

Jira functionalities to note:

- Custom issue types;

- Custom workflows;

- Custom fields;

- Integration with Bitbucket;

- Deployments;

- Releases;

- Components (a default assignee can be defined);

- Time tracking;

- Automation;

- Notifications;

- Issue collectors;

- Reports;

- Saved filters;

- Dashboards;

- Misc:

    - Bulk changes to issues;

    - Dashboards have a "View as wallboard" option;
    
    - Ability to flag issues with an Impediment;
    
    - In issues, "Find your field" helper;

An issue type scheme is associated with a project (or more than one project) to define the issue types available in that project.
