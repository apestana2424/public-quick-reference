# Installation

You can find the installation instructions for several common systems here: [https://help.passbolt.com/hosting/install](https://help.passbolt.com/hosting/install)

If you're installing on a Linux system, check first if firewalld is installed (for example, run `firewall-cmd -h`) and install it if not (`sudo yum install firewalld`).

# User Registration

If Passbolt was configured not to allow public registration, for a user to register an admin first needs to add him to the platform. An email is then sent to the added user with a link to start the registration process:

1. Install the browser extension (this is not optional, the extension must be installed in order to use Passbolt). The extension is available for Chrome and Firefox. After installing the extension, refresh the page as asked.

![Install browser extension](user-registration/install-browser-extension.png)

2. Choose a passphrase. Instead of choosing a "cryptic" word that you won't be able to remember and will have to store somewhere (outside of Passbolt), it is probably a good ideia to choose a phrase that has meaning to you, and that you will be able to remember easily (but that others won't be able to easily guess).

![Passphrase](user-registration/passphrase.png)

3. Download the recovery kit, which is a file containing your secret key. Keep this file somewhere safe.

![Download recovery kit](user-registration/download-recovery-kit.png)

4. Choose a security token. This security token will be shown when Passbolt prompts you for your passphrase - this will help protect you from phishing attacks.

![Choose security token](user-registration/choose-security-token.png)

Done! You will then be redirected to the Passwords page.

![Passwords page](user-registration/passwords-page.png)

# Misc Notes

Even if your user is an Admin, you cannot add users to a Group if you're not a Group Manager of that group. You can, however, change a user on that group from Member to Group Manager.

If there is a group, with passwords, that has a single member and that member is deleted, the group is also deleted.
