# Application Pool Idle Time-out and Recycling Configuration

Select the application pool for which you want to change configuration and access the Advanced Settings (for example, by using the context menu [right-click the application pool]), then:

- For Idle Time-out, in the Process Model section, set the "Idle Time-out (minutes)" property. Set it to zero if you don't want the process to shutdown due to being idle.
- For Recycling, in the Recycling section, set:
    - the "Regular Time Interval (minutes)" property (set it to zero if you don't want regular recycling at a certain interval); or
    - the "Specific Times" property, to have the recycling at specific times of the day.