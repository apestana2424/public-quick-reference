# Installation - PostgreSQL 10 on CentOS 7

To install PostgreSQL 10 on CentOS 7, follow the procedure below (based on [https://cloudwafer.com/blog/installing-postgresql-10-on-centos-7/](https://cloudwafer.com/blog/installing-postgresql-10-on-centos-7/)).

Install the EPEL repo RPM to satisfy dependencies:

`sudo yum install epel-release`

Add the Postgres yum repository:

`sudo rpm -Uvh https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm`

Install PostgreSQL 10 and the contrib package:

`sudo yum install postgresql10-server postgresql10 postgresql-contrib`

Create a new PostgreSQL database cluster by initializing the database with the following command:

`sudo /usr/pgsql-10/bin/postgresql-10-setup initdb`

Start the PostgreSQL service:

`sudo systemctl start postgresql-10.service`

Enable the PostgreSQL service to start on boot:

`sudo systemctl enable postgresql-10.service`

You can check the status of the PostgreSQL service using the following command:

`sudo systemctl status postgresql-10.service`

By default, PostgreSQL does not allow password authentication. Change that by editing PostgreSQL's host-based authenticatio (HBA) configuration. Use the following command to edit the HBA configuration.

`sudo vi /var/lib/pgsql/10/data/pg_hba.conf`

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     all                                     peer
#host    replication     all             127.0.0.1/32            ident
#host    replication     all             ::1/128                 ident
```

Change the postgres OS user password:

`sudo passwd postgres`

After setting the OS user password, change the postgres DB user password:

`su postgres`

`psql`

`ALTER USER postgres WITH PASSWORD '<password>';`

`\q`

To allow remote accesses:

`sudo vi /var/lib/pgsql/10/data/postgresql.conf`

```
## <-- edit listen_addresses line, set as shown below ##
listen_addresses = '*'
```

`sudo vi /var/lib/pgsql/10/data/pg_hba.conf`

```
## <-- add line below, add at the end of the file, place it in a new section "# External connections:" ##
host    all             all             0.0.0.0/0               md5
```

Restart PostgreSQL for configuration changes to take effect:

`sudo systemctl restart postgresql-10.service`


# Dump and Restore

## Dump

Use this command (you will be prompted for the user's password):

`pg_dump -h <host> -U <user> <database> > <file>`

**pg_dump** will output the SQL instructions to create the tables and other database objects and to insert the data into the tables. The command shown redirects the output to a file.

Example:

`pg_dump -h localhost -U postgres suppliers > suppliers.sql`

## Restore

Use this command (you will be prompted for the user's password):

`psql -h <host> -U <user> <database> < <file>`

Or, if you are using a shell that doesn't support the redirection operator:

`psql -h <host> -U <user> <database> -f <file>`

Here we are feeding **psql** with the contents of the file, which contains the SQL instructions to create the tables and other database objects and to insert the data into the tables. Usually, the target database is empty (no tables or other database objects).

Example:

`psql -h localhost -U postgres suppliers_restore < suppliers.sql`

# Terminate Connections

Use the following SQL instruction to terminate connections to a specific database:

`SELECT pg_terminate_backend(pid)`

`FROM pg_stat_activity`

`WHERE datname = '<database>';`
