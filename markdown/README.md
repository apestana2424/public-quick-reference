# This is a Level 1 Heading

## This is a Level 2 Heading

### This is a Level 3 Heading

**This is bold text.**

*This is italic text.*

Here is an ordered list:

1. This is the first item.
2. This is the second item.
3. This is the third item.

Here is an unordered list:

- This is an item.
    - This is a subitem.
- This is another item.
- This is yet another item.

[This is a link.](https://atale.io/)

Here is an image:

![This is an image](image.png)

> This is a block quote.

`This is a code line.`

```
This
is
a code
block.
```

Here is an horizontal rule:

---

Here is a table:

|Column 1  |Column2   |
|----------|----------|
|Value 1.1 |Value 1.2 |
|Value 2.1 |Value 2.2 |